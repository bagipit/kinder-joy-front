const myCheck = document.querySelector('.btn_mycheck'),
  modMyCheck = document.querySelector('.modal_check'),
  btnNewCheck = document.querySelector('.new_check'),
  mod = document.querySelector('.modal'),
  errorMod = document.querySelector('.error'),
  checkTime = document.querySelector('.check_time'),
  add = document.querySelector('.add')
  modlId = document.querySelector('.modal_id'),
  bodyStop = document.querySelector('body'),
  close = document.querySelectorAll('.closed');

myCheck.addEventListener('click', function(e){
  e.preventDefault(),
  modlId.classList.add('show'),
  bodyStop.classList.add('noscroll');
});

btnNewCheck.addEventListener('click', function(e){
  e.preventDefault(),
  mod.classList.add('show'),
  bodyStop.classList.add('noscroll');
});


close.forEach(function(item) {
  item.addEventListener('click', function(e) {
    modMyCheck.classList.remove('show'),
    mod.classList.remove('show'),
    errorMod.classList.remove('show'),
    modlId.classList.remove('show'),
    checkTime.classList.remove('show'),
    bodyStop.classList.remove('noscroll');
  });
});



(function () {
  let currentFile = false

  let checkControl = new Checks
  window.checkControl = checkControl

  $('#modal_send')
      .on('submit', function (e) {
          e.preventDefault()
          if (!currentFile) return alert(window.__lang.empty_file || 'Нет фото чека')

          var paramObj = {};
          $.each($(e.target).serializeArray(), function(_, kv) {
            if (paramObj.hasOwnProperty(kv.name)) {
              paramObj[kv.name] = $.makeArray(paramObj[kv.name]);
              paramObj[kv.name].push(kv.value);
            } else {
              paramObj[kv.name] = kv.value;
            }
          });
          
          formMainParams = new FormData()
          formMainParams.append('fio', paramObj.fio)
          formMainParams.append('phone', paramObj.phone)
          formMainParams.append('code', paramObj.code)
          formMainParams.append('age', paramObj.age)
          formMainParams.append('file', currentFile)
          
          axios.post(
            window.urls.load || '/',
            formMainParams
          ).then(function (res) {
              mod.classList.remove('show')
              modMyCheck.classList.add('show')
              console.log(res.data);
              $('#form_result')
                .text(res.data)
              setTimeout(function (){
                $('#form_result')
                  .text('')
              }, 2400)

          }).catch(function (res) {
            mod.classList.remove('show')
            modMyCheck.classList.add('show')
            console.error(res)
            $('#form_result')
              .text(res.message)
              .addClass('status_error')
            setTimeout(function (){
              $('#form_result')
                .text('')
                .removeClass('status_error')
            }, 2400)
          });
      })
      .find('input[type="file"]')
      .on('change', function (e) {
          const file = e.target.files[0];

          if (!file) return;
          $('[type="submit"]').attr('disabled', 'disabled')
        
          new Compressor(file, {
              quality: 0.6,
              maxWidth: 4192,
              maxHeight: 4192,
              mimeType: 'image/jpeg',
              success(result) {
                currentFile = result;
                $('[type="submit"]').removeAttr('disabled')
                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function () {
                  $('#form_result').text(window.__lang.image_done)
                  setTimeout(function (){
                    $('#form_result')
                      .text('')
                  }, 1200)
                };
                reader.onerror = function (error) {
                    console.log('Error: ', error);
                    $('[type="submit"]').attr('disabled', 'disabled')
                    $('#form_result')
                      .text(window.__lang.image_error)
                      .addClass('status_error')
                    setTimeout(function (){
                      $('#form_result')
                        .text('')
                        .removeClass('status_error')
                    }, 2400)
                };
              },
              error(err) {
                console.log(err.message);
                $('#form_result')
                  .text(window.__lang.image_error)
                  .addClass('status_error')
                setTimeout(function (){
                  $('#form_result')
                    .text('')
                    .removeClass('status_error')
                }, 2400)
                $('[type="submit"]').removeAttr('disabled', 'disabled')
              },
          });
      })

  function copyToClipboard(element) {
      var $temp = $("<input>");
      $("body").append($temp);
      $temp.val($(element).text().trim()).select();
      document.execCommand("copy");
      $temp.remove();
  }

  function Checks() {
    this.block = $('.modal_check')
    this.defaultRow = $('#defaultCheck')
    this.checkList = $('#checkList')
    this.promo = this.block.find('div.input_block > span')
    this.copyBtn = this.block.find('div.input_block > a')

    this.defaultRow.removeAttr('id')
    this.copyBtn.on('click', function (e) {
      e.preventDefault()
      copyToClipboard(this.promo)
      this.copyBtn.addClass('copyed')
      setTimeout(function () {this.copyBtn.removeClass('copyed')}.bind(this), 300)
    }.bind(this))
  }
  Checks.prototype.setPromo = function (newText) {
    this.promo.text(newText)
  }
  Checks.prototype.renderChecks = function (checks) {
    this.checkList.html('')
    $.each(checks, function (i, val) {
      this.renderLine(val)
    }.bind(this))
  }
  Checks.prototype.renderLine = function (data) {
    var check = this.defaultRow.clone(true)
    check.find('span:first-child').text(data.code)
    check.find('span:nth-child(2)').text(window.__lang[data.status])
    if (data.status == 'check_fail') check.find('span:nth-child(2)').addClass('bold_red')
    if (data.status == 'check_success') check.find('span:nth-child(2)').addClass('bold_blue')
    this.checkList.append(check)
    return check;
  }


})()





