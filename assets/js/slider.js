const slider = document.querySelector('.swiper')
      

let swiper = new Swiper('.swiper', {
  
  slidesPerView: 1,

  pagination: {
    el: '.swiper-pagination',
    type: 'bullets',
  },

  navigation: {
    nextEl: '.btn-next',
    prevEl: '.btn-back',
    hashNavigation: true
  },

  scrollbar: {
    el: '.swiper-scrollbar',
  },
});